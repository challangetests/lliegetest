import Vue from 'vue'
import Vuex from 'vuex'
import ProgramasSociais from './modules/programasSociais';
import Pessoas from './modules/pessoas';
import Familias from './modules/familias';

Vue.use(Vuex)

export default new Vuex.Store(
    {
        modules: {
            ps: ProgramasSociais,
            pessoas: Pessoas,
            familia: Familias,
          }
    }
)
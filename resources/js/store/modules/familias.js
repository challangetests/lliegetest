import axios from 'axios';

const Familias = {
    state: {
        familias: [],
        familia: {pessoas: []},
    },
    mutations: {
        familiasIndex(state, data){
            state.familias = data.data
        },
        familiaShow(state, data){
            state.familia = data
        }
    },
    actions: {
        familiaIndex (context) {
            axios.get(`/api/familias`)
            .then(response => {
              context.commit('familiasIndex', response.data)
              context.commit('familiaShow',{data:[]});
            })
        },
        familiaShow (context, id) {
            axios.get(`/api/familias/`+id)
            .then(response => {
              context.commit('familiaShow', response.data)
            })
        },
        familiaCreate ({dispatch , commit}, payload) {
            axios.post(`/api/familias`, payload)
            .then(response => {
                dispatch('familiaIndex');
                commit('familiaShow',{data:[]});
            })
        },
        familiaUpdate ({dispatch , commit}, payload) {
            axios.put(`/api/familias/`+payload.id, payload)
            .then(response => {
                dispatch('pessoas/pessoaIndex', {}, {root:true});
            })
        },
        familiaDestroy ({dispatch}, id) {
            axios.delete(`/api/familias/`+id)
            .then(response => {
                dispatch('familiaIndex');
            })
        },
      }
}

export default Familias;
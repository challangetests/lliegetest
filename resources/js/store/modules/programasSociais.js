import axios from 'axios';

const ProgramasSociais = {
    state: {
        programas_sociais: [],
    },
    mutations: {
        programasSociaisIndex(state, data){
            state.programas_sociais = data.data
        },
    },
    actions: {
        psIndex (context) {
            axios.get(`/api/programas-sociais`)
            .then(response => {
              context.commit('programasSociaisIndex', response.data)
            })
        }
      }
}

export default ProgramasSociais;
import axios from 'axios';

const Pessoas = {
    
    namespaced: true,

    state: {
        pessoas: [],
    },
    mutations: {
        pessoasIndex(state, data){
            state.pessoas = data.data
        },
    },
    actions: {
        pessoaIndex (context) {
            axios.get(`/api/pessoas`)
            .then(response => {
              context.commit('pessoasIndex', response.data)
            })
        }
      }
}

export default Pessoas;
import Vue from 'vue';
import VueRouter from 'vue-router';

import FamiliaIndex from '@/js/components/familia/index';
import FamiliaCreate from '@/js/components/familia/create';
import FamiliaEdit from '@/js/components/familia/edit';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'familia-index',
            component: FamiliaIndex,
        },
        {
            path: '/familia/create',
            name: 'familia-create',
            component: FamiliaCreate,
        },
        {
            path: '/familia/edit/:id',
            name: 'familia-edit',
            component: FamiliaEdit,
        },
    ]
})

export default router;
import './bootstrap';
import Vue from 'vue';
import store from './store'
import Vuetify from 'vuetify';

import Routes from '@/js/routes.js'; 

import App from '@/js/views/App'; 


Vue.use(Vuetify);

const app = new Vue({
    el: '#app',
    router: Routes,
    store,
    vuetify: new Vuetify({
        theme: {
          themes: {
            light: {
              primary: '#3f51b5',
              secondary: '#b0bec5',
              accent: '#8c9eff',
              error: '#b71c1c',
              nav: '#0ec4b8',
              bg: '#c7d6d5',
            },
          },
        },
      }),
    render: h => h(App),
});

export default app;
# lliegetest

Teste para vaga na LLIEGE

1. Instale os containers com o docker-compose.yml na raiz do projeto.
2. Entre no docker do php72-fpm e rode os comandos:
    2.1 composer install 
    2.2 php artisan migrate:fresh --seed
    2.2 npm run dev
3. gere o .env do projeto e sete as credenciais do banco de dados.
4. configure lliegetest.desenv no hosts para o localhost.
5. acesse lliegetest.desenv para testar a aplicação.

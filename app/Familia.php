<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $table = 'familia';

    protected $fillable =[
        'estado', 'cidade', 'bairro', 'cep', 
        'logradouro','num_logradouro','id_programa',
    ];

    public function programa(){
        return $this->belongsTo('App\ProgramaSocial', 'id_programa');
    }

    public function pessoas(){
        return $this->hasMany('App\Pessoa', 'familia_id');
    }
}

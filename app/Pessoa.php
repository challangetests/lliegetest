<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table = 'pessoa';

    public $timestamps = false;

    public function familia(){
        return $this->belongsTo('App\Familia', 'familia_id');
    }
}

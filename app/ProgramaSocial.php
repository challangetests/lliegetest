<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaSocial extends Model
{
    protected $table = 'programa_social';
}

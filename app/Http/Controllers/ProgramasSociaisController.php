<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProgramaSocial;
use App\Http\Resources\ProgramasSociais as ProgramasSociaisResource;

class ProgramasSociaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  ProgramasSociaisResource::collection(ProgramaSocial::all());
    }
}

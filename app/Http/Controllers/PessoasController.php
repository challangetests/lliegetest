<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Pessoas as PessoasResource;
use App\Pessoa;

class PessoasController extends Controller
{
    public function index()
    {   
        return  PessoasResource::collection(Pessoa::doesntHave('familia')->get());
    }
}

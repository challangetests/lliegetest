<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Familia;
use App\Pessoa;

use App\Http\Resources\Familias as FamiliasResource;

class FamiliasController extends Controller
{
    public function index()
    {
        return  FamiliasResource::collection(Familia::all());
    }

    public function store(Request $request)
    {
        $pessoas = collect($request->pessoas)->pluck('id');
        $familia = Familia::create($request->all());
        $familia->pessoas()->update(['familia_id' => null]);
        $familia->pessoas()->saveMany(Pessoa::whereIn('id', $pessoas)->get());

        return [
            "success" => true,
            "message" => 'Familia Cadastrada com sucesso'
        ];
    }

    public function show($id)
    {
        return Familia::whereId($id)->with('pessoas')->first();
    }

    public function update(Request $request, $id)
    {
        $pessoas = collect($request->pessoas)->pluck('id');
        $familia = Familia::find($id);

        $familia->pessoas()->update(['familia_id' => null]);
        $familia->pessoas()->saveMany(Pessoa::whereIn('id', $pessoas)->get());

        $familia->fill($request->all())->save();
        
        return  [
            "success" => true,
            "message" => 'Familia Atualizada com sucesso'
        ];
    }

    public function destroy($id)
    {
        Familia::find($id)->delete();

        return [
            "success" => true,
            "message" => 'Familia Deletada com sucesso'
        ];
    }
}

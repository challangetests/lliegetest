<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Familias extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'estado' => $this->estado,
            'cidade' => $this->cidade,
            'bairro' => $this->bairro,
            'cep' => $this->cep,
            'logradouro' => $this->logradouro,
            'num_logradouro' => $this->num_logradouro,
            'programa' => $this->programa,
            'pessoas' => implode(';',$this->pessoas->pluck('nome')->toArray()),
        ];
    }
}

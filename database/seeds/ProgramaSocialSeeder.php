<?php

use Illuminate\Database\Seeder;

class ProgramaSocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programa_social')->insert([
            ['nome_programa' => 'Bolsa Família'],
            ['nome_programa' => 'Renda cidadã'],
            ['nome_programa' => 'Minha casa minha vida'],
        ]);
    }
}

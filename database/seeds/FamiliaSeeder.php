<?php

use Illuminate\Database\Seeder;

class FamiliaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('familia')->insert([
            [
                'estado' => 'SP',
                'cidade' => 'São Paulo',
                'bairro' => 'Pinheiros',
                'cep' => '05547030',
                'logradouro' => 'Rua dos Pardais',
                'num_logradouro' => 3306,
                'id_programa' => 1,
            ],
            [
                'estado' => 'RS',
                'cidade' => 'Rio de Janeiro',
                'bairro' => 'Barra da Tijuca',
                'cep' => '05547031',
                'logradouro' => 'Avenida dos Pardais',
                'num_logradouro' => 8080,
                'id_programa' => 2,
            ],
            [
                'estado' => 'BA',
                'cidade' => 'Salvador',
                'bairro' => 'João Pessoa',
                'cep' => '05547032',
                'logradouro' => 'Alameda dos Pardais',
                'num_logradouro' => 403,
                'id_programa' => 3,
            ],
        ]);
    }
}

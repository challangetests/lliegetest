<?php

use Illuminate\Database\Seeder;

class PessoaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pessoa')->insert([
            [
                'familia_id' => null,
                'nome' => 'João Carlos Amorim',
                'sexo' => 'M',
                'data_nascimento' => '1996-03-10',
                'naturalidade' => 'Brasileiro',
                'cpf' => 79159458070,
                'rg' => 165404759,
                'estado_civil' => 'solteiro',
            ],
            [
                'familia_id' => null,
                'nome' => 'Michele de Souza',
                'sexo' => 'M',
                'data_nascimento' => '1993-03-13',
                'naturalidade' => 'Brasileira',
                'cpf' => '03432824025',
                'rg' => 486034495,
                'estado_civil' => 'solteira',
            ],
            [
                'familia_id' => 1,
                'nome' => 'João da Silva',
                'sexo' => 'M',
                'data_nascimento' => '1995-06-22',
                'naturalidade' => 'Brasileiro',
                'cpf' => 58226715008,
                'rg' => 308031994,
                'estado_civil' => 'solteiro',
            ],
            [
                'familia_id' => 1,
                'nome' =>  'Maria da Silva',
                'sexo' => 'F',
                'data_nascimento' => '1991-07-12',
                'naturalidade' => 'Brasileira',
                'cpf' => 45117272013,
                'rg' => 410572366,
                'estado_civil' => 'casada',
            ],
            [
                'familia_id' => 1,
                'nome' =>  'Henrique da Silva',
                'sexo' => 'M',
                'data_nascimento' => '2010-01-10', 
                'naturalidade' => 'Brasileiro',
                'cpf' => 88612751098,
                'rg' => 191925639,
                'estado_civil' => 'solteiro',
            ],
            [
                'familia_id' => 2,
                'nome' =>  'Daniel de Souza',
                'sexo' => 'M',
                'data_nascimento' => '1986-07-20', 
                'naturalidade' => 'Brasileiro',
                'cpf' => 73556689006,
                'rg' => 243680648,
                'estado_civil' => 'casado',
            ],
            [
                'familia_id' => 2,
                'nome' =>  'Gabriela de Souza',
                'sexo' => 'M',
                'data_nascimento' => '1982-06-23', 
                'naturalidade' => 'Brasileira',
                'cpf' => 26241701037,
                'rg' => 174829553,
                'estado_civil' => 'casada',
            ],
            [
                'familia_id' => 2,
                'nome' =>  'Ana de Souza',
                'sexo' => 'M',
                'data_nascimento' => '1990-10-27', 
                'naturalidade' => 'Brasileira',
                'cpf' => '02498380019',
                'rg' => 335002493,
                'estado_civil' => 'casada',
            ],
            [
                'familia_id' => 3,
                'nome' =>  'Juliana Peres',
                'sexo' => 'M',
                'data_nascimento' => '1976-11-12', 
                'naturalidade' => 'Brasileira',
                'cpf' => 18522575045,
                'rg' => 270381508,
                'estado_civil' => 'solteira',
            ],
            [
                'familia_id' => 3,
                'nome' =>  'Paulo Peres',
                'sexo' => 'M',
                'data_nascimento' => '1989-01-11', 
                'naturalidade' => 'Brasileiro',
                'cpf' => 59697662088,
                'rg' => 485824541,
                'estado_civil' => 'solteiro',
            ],
        ]);
    }
}

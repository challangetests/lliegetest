<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('familia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('estado', 2);
            $table->string('cidade');
            $table->string('bairro');
            $table->integer('cep');
            $table->string('logradouro');
            $table->integer('num_logradouro');
            $table->bigInteger('id_programa')->unsigned();

            $table->foreign('id_programa')->references('id')->on('programa_social')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('familia');
    }
}

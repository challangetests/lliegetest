<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('familia_id')->nullable()->unsigned();
            $table->string('nome');
            $table->char('sexo', 1);
            $table->date('data_nascimento');
            $table->string('naturalidade');
            $table->bigInteger('cpf');
            $table->integer('rg');
            $table->string('estado_civil');

            $table->foreign('familia_id')->references('id')->on('familia')->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoa');
    }
}
